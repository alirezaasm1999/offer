let Data = [
  {
    name: "نان مغزدار شکلاتی(کروسان)",
    description: "غرفه:خانه کیک آیناز",
    img: "../assests/product-1.jpg",
    off: "20%",
    oldPrice: "48,000",
    newPrice: "36,000",
  },
  {
    name: "باقلوا",
    description: "غرفه:شیرینی حاج شهرام یزد",
    img: "../assests/product-2.jpg",
    off: "27%",
    oldPrice: "54,000",
    newPrice: "39,500",
  },
  {
    name: "توت خشک ارگانیک و محلی",
    description: "غرفه:ادویه و خشکبار نازگل",
    img: "../assests/product-3.jpg",
    off: "16%",
    oldPrice: "58,000",
    newPrice: "48,500",
  },
  {
    name: "رومیزی ترمه بختیاری",
    description: "غرفه:ترمه النا یزد",
    img: "../assests/product-4.jpg",
    off: "34%",
    oldPrice: "70,000",
    newPrice: "46,000",
  },
  {
    name: "زیتون درجه یک فدک",
    description: "غرفه:ترمه النا یزد",
    img: "../assests/product-5.jpg",
    off: "6%",
    oldPrice: "18,000",
    newPrice: "17,000",
  },
  {
    name: "زعفران نگین 1 مثقالی",
    description: "غرفه:محصولات روستایی سبزرود",
    img: "../assests/product-6.jpg",
    off: "34%",
    oldPrice: "65,000",
    newPrice: "43,000",
  },
];

document.getElementById("content").innerHTML = Data.map(
  (item) =>
    `  <div class="card">
              <div class="image">
                <img src=${item.img} />
              </div>
              <div class="productInfo">
                <p>${item.name}</p>
                <p>${item.description}</p>
              </div>
              <div class="offPrice">
                <div>
                  <span id="countOfOff">${item.off}</span>
                  <span id="oldPrice">${item.oldPrice}</span>
                </div>
                <div>
                  <img src="../assests/toman.svg" id="toman" />
                  <span id="newPrice">${item.newPrice}</span>
                  <img
                    src="../assests/more.png"
                    width="14px"
                    height="18px"
                    id="more"
                  />
                </div>
              </div>
            </div>`
).join(" ");

setInterval(() => {
  let date = new Date();
  document.getElementById("hour").innerHTML = date.getHours();
  document.getElementById("min").innerHTML = date.getMinutes();
  document.getElementById("sec").innerHTML = date.getSeconds();
}, 1000);
